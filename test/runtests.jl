
using AudioSources
using Test

const URL = "https://catalog.ldc.upenn.edu/desc/addenda/LDC93S1.wav"
const FILE = basename(URL)

# Download the file for the tests if not done already.
if ! ispath("$FILE")
    run(`wget -O $FILE $URL`)
end

tmp = FileAudioSource("$FILE")
data, srate = load(tmp)

sources = [
    "file" => FileAudioSource("$FILE"),
    # This fails on the runner as the file is not found.
    #"cmd" => CmdAudioSource(`sox -r 16000 $FILE -t wav -`),
    "raw" => RawAudioSource(randn(46797, 1), 16000),
    "url" => URLAudioSource("$URL")
]

for (name, src) in sources
    @testset "$name" begin
        data, srate = load(src)
        @test srate == 16000
        @test size(data) == (46797, 1)

        data, srate = load(src, subrange = 1000:2000)
        @test srate == 16000
        @test size(data) == (1001, 1)

        data, srate = load(src, subrange = 1000:2000, channels = 1)
        @test srate == 16000
        @test size(data) == (1001,)
    end
end


@testset "html display" begin

    # Ensure that the output val
    base64str = AudioSources.html_audiosource(RawAudioSource(zeros(1000), 16000))
    @test base64str isa AbstractString
    @test startswith(base64str, "data:audio/wav;base64")
end
