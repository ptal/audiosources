# Installation

This package is part of the PTAL tool collection and requires the
[PTAL registry](https://gitlab.lisn.upsaclay.fr/ptal/registry) to be installed.

To add this registry to your Julia installation type `]` to enter the
package mode of the REPL and then type:

```
pkg> registry add "https://gitlab.lisn.upsaclay.fr/PTAL/Registry"
```

Once the registry has been added, AudioSources can be installed with the
Julia package manager by typing in Pkg REPL mode
```
pkg> add AudioSources
```
