# AudioSources.jl

AudioSources.jl is a Julia package for loading WAV-formatted resources: file,
URL, shell command and raw data. 

## Contents

```@contents
Pages = ["index.md", "installation.md", "examples.md", "api.md"]
```

## License
This software is provided under the [CeCILL-B license](https://cecill.info/licences.en.html)

## Authors

- Lucas Ondel Yang
- Nicolas Denier

![](https://ptal.lisn.upsaclay.fr/assets/lisn-ups-cnrs.png)

