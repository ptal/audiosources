# API
## Sources
Each `AudioSource` is a subtype of `AbstractAudioSource` and allows to load audio data in a unified format. 

```@docs
AbstractAudioSource
```
### Command
```@docs
CmdAudioSource
```
### File
```@docs
FileAudioSource
```
### Raw
```@docs
RawAudioSource
```
### URL 
```@docs
URLAudioSource
```

## Functions
```@docs
load
Base.show(io::IO, ::MIME"text/html", s::AbstractAudioSource)
```

## Index

```@index
```
