# Examples

## Get data from a file
```julia
using AudioSources
source = FileAudioSource("path/to/data.wav")
x, fs = load(source)
```
Similarly, you can load any `AudioSource` and get the data as a Julia `Array` (`x` in the example).

## SpeechFeatures overwrite
If you load [`SpeechFeatures`](https://ptal.lisn.upsaclay.fr/SpeechFeatures/) and add `true` to the load function, you will get a `FeaturesProperties` object instead of just the sampling rate for the  second returned value. This way, the result of load can directly be piped into a features extraction pipeline.
```julia
using AudioSources, SpeechFeatures
source = URLAudioSource("https://your.favorite.song.url")
x, p = load(source, true)
```

## SpeechDatasets overwrite
You can also load directly a [`SpeechDataset`](https://ptal.lisn.upsaclay.fr/SpeechDatasets/) item. That is because a `Recording` object stores an `AudioSource` one, whether it is from a file, or a command.

```julia
using SpeechDatasets
ds = dataset(...)
x, p = load(ds[1])
```