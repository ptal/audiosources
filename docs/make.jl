push!(LOAD_PATH,"..")

using Documenter, AudioSources
using Documenter.Remotes

include("deployconfig.jl")

DocMeta.setdocmeta!(AudioSources, :DocTestSetup, :(using AudioSources); recursive = true)

makedocs(
    sitename="AudioSources",
    repo = Remotes.GitLab("gitlab.lisn.upsaclay.fr", "PTAL", "AcousticProcessing/AudioSources.jl.git"),
    doctest = false,
    modules = [AudioSources],
    pages = [
        "Home" => "index.md",
        "Installation" => "installation.md",
        "Examples" => "examples.md",
        "API" => "api.md"
    ]
)

config = GitLabHTTPS()

deploydocs(
    repo = "gitlab.lisn.upsaclay.fr/PTAL/AcousticProcessing/AudioSources.jl.git",
    branch = "docs",
    deploy_config = GitLabHTTPS(),
    versions=["latest"=>"v^", "v#.#.#"]
)
