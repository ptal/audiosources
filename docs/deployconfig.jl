# Allows to connect to GitLab with HTTPS
# https://github.com/JuliaDocs/Documenter.jl/issues/2061#issuecomment-1607077792
# https://github.com/JuliaDocs/Documenter.jl/blob/master/src/deployconfig.jl
# Adapted to have versionned docs with PTAL CICD (get version from Project.toml instead of tag, as it occurs during build, not deploy)
using Documenter: DeployConfig, DeployDecision, marker, env_nonempty, HTTPS

PROJECT_TOML="Project.toml" # Note that it is the package Project.toml, not the docs one
VERSION_PATTERN=r"[0-9]*\.[0-9]*\.[0-9]*"

# Extract version number from Project.toml 
function get_version()
    versionline = open(PROJECT_TOML) do f
        filter(line -> occursin("version", line), readlines(f))[1]
    end
    "v" * match(VERSION_PATTERN, versionline).match
end

@kwdef struct GitLabHTTPS <: DeployConfig
    repo_path::String = get(ENV, "CI_PROJECT_PATH", "")
    repo_slug::String = get(ENV, "CI_PROJECT_PATH_SLUG", "")
    pipeline_source::String = get(ENV, "CI_PIPELINE_SOURCE", "")
    package_version::String = get_version()
end

Documenter.authentication_method(::GitLabHTTPS) = HTTPS

function Documenter.authenticated_repo_url(cfg::GitLabHTTPS) 
    token = get(ENV,"CI_BOT_TOKEN","")
    host = get(ENV,"CI_SERVER_HOST", "")
    return "https://documenter-ci:$(token)@$(host)/$(cfg.repo_path).git"
end

function Documenter.deploy_folder(
    cfg::GitLabHTTPS;
    repo,
    branch = "docs",
    kwargs...,
)
    io = IOBuffer()
    all_ok = true

    println(io, "\nGitLab config:")
    println(io, "  Repo slug: \"", cfg.repo_slug, "\"")
    println(io, "  Pipeline source: \"", cfg.pipeline_source, "\"")
    println(io, "  Package version: \"", cfg.package_version, "\"")

    version_ok = cfg.package_version !== nothing
    println(io, "- $(marker(version_ok)) Package version was found")
    all_ok &= version_ok
    subfolder = cfg.package_version

    key_ok = env_nonempty("CI_BOT_TOKEN")
    println(io, "- $(marker(key_ok)) ENV[\"CI_BOT_TOKEN\"] exists and is non-empty")
    all_ok &= key_ok

    print(io, "Deploying to folder $(repr(subfolder)): $(marker(all_ok))")
    @info String(take!(io))

    return DeployDecision(;
        all_ok = all_ok,
        branch = branch,
        repo = repo,
        subfolder = subfolder,
        is_preview = false,
    )

end