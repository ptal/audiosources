# AudioSources.jl

Julia package to load WAV-formatted sources. AudioSources.jl is part of
[PTAL]("https://ptal.lisn.upsaclay.fr/").

## Installation

Make sure to add the [PTAL registry](https://gitlab.lisn.upsaclay.fr/PTAL/Registry)
to your julia installation. Then, install the package as usual:
```julia
pkg> add AudioSources
```

## Usage

```julia
using AudioSources

src = URLAudioSource("https://catalog.ldc.upenn.edu/desc/addenda/LDC93S1.wav")
data, srate = load(src)
```

For more information, have a look at the [documentation]("https://ptal.lisn.upsaclay.fr/AudioSources/").

