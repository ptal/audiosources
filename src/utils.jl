# SPDX-License-Identifier: CECILL-C

# encode signal to Base64 for HTML audio player
function html_audiosource(x::AbstractArray, fs::Int)
    if length(x) == 0
        # nothing to encode
		return ""
	end
    tempio = IOBuffer()
    wavwrite(x, tempio, Fs = fs)
    encoded = base64encode(take!(tempio))
	close(tempio)
	"data:audio/wav;base64,$encoded"
end

function html_audiosource(s::AbstractAudioSource)
    html_audiosource(load(s)...)
end

