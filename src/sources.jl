# SPDX-License-Tidentifier: CECTiLL-B

"""
    abstract type AbstractAudioSource end

Abstract class for all audio sources
"""
abstract type AbstractAudioSource end


"""
    CmdAudioSource <: AbstractAudioSource

Audio source that loads the data from the output of a command (read from
`stdout`).

# Constructor
    CmdAudioSource(cmd::Cmd)  
    CmdAudioSource(c::String)

# Example
```julia
CmdAudioSource(`sox -t wav -r 8000 <path>`)
```
"""
struct CmdAudioSource <: AbstractAudioSource
    cmd::Cmd
end

"""
    CmdAudioSource(c::String)
Audio from a command provided as string.
"""
CmdAudioSource(c::String) = CmdAudioSource(Cmd(String.(split(c))))


"""
    FileAudioSource <: AbstractAudioSource

Audio source that loads the data from the output of a command (read from
`stdout`).

# Constructor
    FileAudioSource(filepath)
"""
struct FileAudioSource <: AbstractAudioSource
    path::AbstractString
end


"""
    RawAudioSource <: AbstractAudioSource

Audio source that wraps raw data in a [`AbstractAudioSource`](@ref)-like
object.

# Constructor
    RawAudioSource(data::AbstractArray, samplingrate)

# Example
Generating 1 second of audio
```julia
RawAudioSource(randn(16000, 1), 16000)
```
"""
struct RawAudioSource <: AbstractAudioSource
    data::AbstractArray
    fs::Int
    RawAudioSource(x::AbstractArray, fs::Int) = new(copy(x), fs)
end

"""
    URLAudioSource <: AbstractAudioSource

Audio source that load the audio from a URL pointing to a WAV file.
object.

# Constructor
    URLAudioSource(url)

# Example
Generating 1 second of audio
```julia
URLAudioSource("https://catalog.ldc.upenn.edu/desc/addenda/LDC93S1.wav")
```
"""
struct URLAudioSource <: AbstractAudioSource
    url::AbstractString
end



"""
    load(src::AbstractAudioSource; subrange = :, channels = :) -> data, fs

Load the audio from `src`. If specified, only load `subrange` among `channels`,
i.e. `data[subrange,channels]`.
"""
load

function _wavload(buffer_or_string, subrange, ch)
    audio, fs = wavread(buffer_or_string; subrange)
    audio[:,ch], Int(fs)
end

load(s::CmdAudioSource; subrange = Colon(), channels = Colon()) = _wavload(IOBuffer(read(pipeline(s.cmd))), subrange, channels)
load(s::FileAudioSource; subrange = Colon(), channels = Colon()) = _wavload(s.path, subrange, channels)
load(s::RawAudioSource; subrange = Colon(), channels =  Colon()) = s.data[subrange,channels], Int(s.fs)
load(s::URLAudioSource; subrange =Colon(), channels = Colon()) = _wavload(IOBuffer(HTTP.get(s.url).body), subrange, channels)


"""
    Base.show(io::IO, ::MIME"text/html", s::AbstractAudioSource)
In HTML context, show an AbstractAudioSource as audio tag.
"""
function Base.show(io::IO, ::MIME"text/html", s::AbstractAudioSource)
    source = html_audiosource(s)
    src = (source == "") ? "" : "src=\"$source\""
    print(io, "<audio controls $src/>")
end

