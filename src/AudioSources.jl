# SPDX-License-Identifier: CECILL-C

module AudioSources

using Base64: base64encode
using HTTP
using WAV

export AbstractAudioSource,
    CmdAudioSource,
    FileAudioSource,
    RawAudioSource,
    URLAudioSource,
    load


include("sources.jl")
include("utils.jl")

end
